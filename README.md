# Python

<img src="https://gitlab.com/pascal.cantaluppi/python/-/raw/main/img/python.png" />

Python playground

1. Basics
2. Control structures
3. Functions
4. Lists

## Getting started

Download Anaconda

<img src="https://gitlab.com/pascal.cantaluppi/python/-/raw/main/img/anaconda.png" />

[https://www.anaconda.com/](https://www.anaconda.com/)

Start Jupyter Notebook

<img src="https://gitlab.com/pascal.cantaluppi/python/-/raw/main/img/jupyter.png" />
